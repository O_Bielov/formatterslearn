//
//  MinutesFormatter.swift
//  FormattersLearn
//
//  Created by Oleksandr Bielov on 25.01.2022.
//

import UIKit

class MinutesFormatter: DateFormatter {

  override func string(from date: Date) -> String {
    let currentDate = Date()
    let differenceInMinutes = (currentDate.timeIntervalSince1970 - date.timeIntervalSince1970) / 1000 / 60
    // currentDate.timeIntervalSince1970 - currentDate.timeIntervalSince1970 -- miliseconds
    // /1000 -- seconds
    // /60 -- minutes
    
    if(differenceInMinutes < 60){
      let formatString = NSLocalizedString(
        "Minutes count",
        comment: "Minutes count in stringdict")
      
      return String(format: formatString, Int(differenceInMinutes))
    }
    
    return ""
  }
  
}
