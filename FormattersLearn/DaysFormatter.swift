//
//  DaysFormatter.swift
//  FormattersLearn
//
//  Created by Oleksandr Bielov on 25.01.2022.
//

import UIKit

class DaysFormatter: DateFormatter {

  override func string(from date: Date) -> String {
    let currentDate = Date()
    let differenceInDays = (currentDate.timeIntervalSince1970 - date.timeIntervalSince1970) / 1000 / 60 / 60 / 24
    // currentDate.timeIntervalSince1970 - currentDate.timeIntervalSince1970 -- miliseconds
    // /1000 -- seconds
    // /60 -- minutes
    // /60 -- hours
    // /24 -- days
    
    let formatString = NSLocalizedString(
      "Days count",
      comment: "Days count in stringdict")
    
    return String(format: formatString, Int(differenceInDays))
  }
  
}
