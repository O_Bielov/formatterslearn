//
//  ViewController.swift
//  FormattersLearn
//
//  Created by Oleksandr Bielov on 25.01.2022.
//

import UIKit

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    
    let dateFormatter = DateFormatter()
    let date = Date()
    
    dateFormatter.dateFormat = "hh:MM a"
    print(dateFormatter.string(from: date))
    
    dateFormatter.dateFormat = "HH:MM:ss"
    print(dateFormatter.string(from: date))
    
    dateFormatter.dateFormat = "E, MMM d, yyyy"
    print(dateFormatter.string(from: date))
    
    
    // MARK: Custom formatter
    let customFormatter = CustomTimeFormatter()
    let oneMinute = 1000 * 60
    
    print(customFormatter.string(
      from: Date(timeInterval: -Double(oneMinute) * 0,
                 since: Date())))
    
    print(customFormatter.string(
      from: Date(timeInterval: -Double(oneMinute),
                 since: Date())))
    
    print(customFormatter.string(
      from: Date(timeInterval: -Double(oneMinute * 3),
                 since: Date())))
    
    print(customFormatter.string(
      from: Date(timeInterval: -Double(oneMinute * 21),
                 since: Date())))
    
    print(customFormatter.string(
      from: Date(timeInterval: -Double(oneMinute * 10),
                 since: Date())))
    
    print(customFormatter.string(
      from: Date(timeInterval: -Double(oneMinute * 50),
                 since: Date())))
    
    let oneHour = oneMinute * 60
    
    print(customFormatter.string(
      from: Date(timeInterval: -Double(oneHour),
                 since: Date())))
    
    print(customFormatter.string(
      from: Date(timeInterval: -Double(oneHour * 3),
                 since: Date())))
    
    print(customFormatter.string(
      from: Date(timeInterval: -Double(oneHour * 10),
                 since: Date())))
    
    print(customFormatter.string(
      from: Date(timeInterval: -Double(oneHour * 23),
                 since: Date())))
    
    let oneDay = oneHour * 24
    
    print(customFormatter.string(
      from: Date(timeInterval: -Double(oneDay),
                 since: Date())))
    
    print(customFormatter.string(
      from: Date(timeInterval: -Double(oneDay * 3),
                 since: Date())))
    
    
    print("")
    // MARK: Translate phrase
    
    for n in 0...25{
      let format = NSLocalizedString("Friends count", comment: "dictstring")
      print(String(format: format, n))
    }
    
  }


}

