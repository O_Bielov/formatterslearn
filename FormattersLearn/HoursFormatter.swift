//
//  HoursFormatter.swift
//  FormattersLearn
//
//  Created by Oleksandr Bielov on 25.01.2022.
//

import UIKit

class HoursFormatter: DateFormatter {

  override func string(from date: Date) -> String {
    let currentDate = Date()
    let differenceInHours = (currentDate.timeIntervalSince1970 - date.timeIntervalSince1970) / 1000 / 60 / 60
    // currentDate.timeIntervalSince1970 - currentDate.timeIntervalSince1970 -- miliseconds
    // /1000 -- seconds
    // /60 -- minutes
    // /60 -- hours
    
    if(differenceInHours < 24){
      let formatString = NSLocalizedString(
        "Hours count",
        comment: "Hours count in stringdict")
      
      return String(format: formatString, Int(differenceInHours))
    }
    
    return ""
  }
  
}
