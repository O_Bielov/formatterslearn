
//
//  CustomTimeFormatter.swift
//  FormattersLearn
//
//  Created by Oleksandr Bielov on 25.01.2022.
//

import Foundation

class CustomTimeFormatter: DateFormatter {
  
  private let minutesFormatter = MinutesFormatter()
  private let hoursFormatter = HoursFormatter()
  private let daysFormatter = DaysFormatter()
  
  
  override func string(from date: Date) -> String {
    let curDate = Date()
    let differenceInMinutes = (curDate.timeIntervalSince1970 - date.timeIntervalSince1970) / 1000 / 60
    
    if (differenceInMinutes < 60) {
      return getMinute(from: date)
    }
    else if (differenceInMinutes < 60 * 24) {
      return getHour(from: date)
    }
    return getDay(from: date)
  }
  
  private func getDay(from date: Date) -> String {
    daysFormatter.string(from: date)
  }
  
  private func getHour(from date: Date) -> String {
    let result = hoursFormatter.string(from: date)
    return result == "" ? getDay(from: date) : result
  }
  
  private func getMinute(from date: Date) -> String {
    let result = minutesFormatter.string(from: date)
    return result == "" ? getHour(from: date) : result
  }
  
}
